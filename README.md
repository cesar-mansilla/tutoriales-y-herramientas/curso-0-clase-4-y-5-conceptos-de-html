# Ejercicio 1. Estructura HTML.
Este repositorio corresponde al video de prácticas de HTML planteado en: 
- [Conceptos de HTML + 🎁 Regalos! | Clase 4](https://youtu.be/gFunCM5qurQ "Youtube")
- [Explicación del ejercicio de HTML | Clase 5](https://youtu.be/fyHhEoonh74 "Youtube")

## Consigna.
---
Deberás implementar el uso de etiquetas pensando como bloques el diseño de la imagen adjunta. **No te preocupes por los colores o que todo quede acomodado en su lugar**. 

> Recordá que HTML se encarga de la estructura, y este ejercicio se trata de HTML.

### Página Home
---

#### Debe contener:
- Imagen de logo almacenado en alguna carpeta del proyecto
- Distintos encabezados
- Párrafos
- Imagen a elección que sea externa al sitio
- Links internos
- Links externos

<img src="./imagenes/home.png" width="960px" />

### Página Contacto
---

#### Debe contener:
- Distintos tipos de inputs
  * Texto
  * Número
  * Selector
  * Radio
  * Checkbox
  * Caja de texto

<img src="./imagenes/contacto.png" width="960px" />

## Material.
---
En el repositorio encontrarás:
- Logo
- Diseño
- Archivos HTML de referencia

| Archivo | Comentarios |
| - | - |
| index.html | HTML básico, pero funcional. |
| index-estructurado.html | HTML mejorado, ya hay contenedores, por lo que se cumple la consigna de pensar el diseño como bloques. Deberías apuntar a lograr esto. |
| index-semantico.html | HTML correcto, si lográste acercarte a esta versión genial! |
| index-semantico-comentado.html | Es el mismo HTML semántico, sólo que dejé comentarios en varias líneas, por si no lograste resolverlo así, pero te interesaría hacerlo. |